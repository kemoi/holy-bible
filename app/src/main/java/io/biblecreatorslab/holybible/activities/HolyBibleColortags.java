package io.biblecreatorslab.holybible.activities;

import android.content.Intent;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import io.biblecreatorslab.holybible.R;
import io.biblecreatorslab.holybible.adapters.ColortagAdapter;
import io.biblecreatorslab.holybible.data.ColortagsHelper;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

public class HolyBibleColortags extends AppCompatActivity {
    private InterstitialAd interstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_holybible_colortags);
        showAllColorTags();

        MobileAds.initialize(this,getString(R.string.APP_ID));

        interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId(getString(R.string.interstitial_full_screen));
        interstitialAd.loadAd(new AdRequest.Builder().build());

        interstitialAd.setAdListener(new AdListener() {
            public void onAdLoaded() {
                showInterstitial();
            }
        });
    }
//    custom methods
    private void showAllColorTags() {
        //lets connect to our table and get all bookmarks
        ColortagsHelper colortagsHelper = new ColortagsHelper(this);

        Cursor cursor = colortagsHelper.findAll();

        CursorAdapter cAdapter = new ColortagAdapter(this,cursor,0);
        final ListView listView1 = (ListView) findViewById(android.R.id.list);
        listView1.setAdapter(cAdapter);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.hb_colortags, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //events
    //events

    public void onHome(View v){
        Intent intent = new Intent(this, HolyBibleHome.class);
        startActivity(intent);
    }

    public void onBookSelect(View v){
        Intent intent = new Intent(this, HolyBibleBooksAll.class);
        startActivity(intent);
    }
    public void onSearch(View v){
        //start the intent and search activity
        Intent intent = new Intent(this, HolyBibleSearchPage.class);
        startActivity(intent);
    }

    private void showInterstitial() {
        if (interstitialAd.isLoaded()) {
            interstitialAd.show();
        }
    }
}
